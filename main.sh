#!/bin/bash
###### Partition selection menu ######
select_partition(){
    export PATH="/bin:/sbin:/usr/bin:/usr/sbin"
    menu=()
    for disk in /sys/block/* ; do
        disk=${disk/*\//}
        for part in /sys/block/$disk/$disk* ; do
            if [[ -d $part ]] ; then
                part=${part/*\//}
                type=$(blkid | grep "/dev/$part:" | sed "s/.* TYPE=\"//g;s/\".*//g")
                label=$(blkid | grep "/dev/$part:" | grep LABEL | sed "s/.* LABEL=\"//g;s/\".*//g")
                if [[ "$label" == "" ]] ; then
                    label="-"
                fi
                size=$(lsblk -r | grep "^$part " | cut -f4 -d" ")
                menu+=("$part" "$type	$label	$size")
            fi
        done
    done >/dev/null
    create_label="Create/Modify partitions"
    info_label="Partition | Filesystem | Label | Size"
    result=$(dialog --no-cancel \
        --output-fd 1 --menu \
        "$TITLE\n\n${info_label}" 0 0 0 \
        "${menu[@]}" \
         "${create_label}" "")
    if [[ "$result" == "${create_label}" ]] ; then
        echo create
    else
        echo $result
        #return $result
    fi 

}

###### Disk selection menu ######
select_disk(){
HEIGHT=15
WIDTH=40
CHOICE_HEIGHT=4

    export PATH="/bin:/sbin:/usr/bin:/usr/sbin"
    menu=()
    for disk in /sys/block/* ; do
        disk=${disk/*\//}
        size=$(lsblk -r | grep "^$disk " | cut -f4 -d" ")
        menu+=("$disk" "$size")
    done >/dev/null
    result=$(dialog --no-cancel \
        --output-fd 1 --menu \
        "$TITLE" $HEIGHT $WIDTH $CHOICE_HEIGHT \
        "${menu[@]}")
    echo $result
}

select_disk1(){
HEIGHT=15
WIDTH=40
CHOICE_HEIGHT=4
BACKTITLE="Backtitle here"
TITLE="Title here"
MENU="Choose one of the following options:"

menu=()
    for disk in /sys/block/* ; do
        disk=${disk/*\//}
        size=$(lsblk -r | grep "^$disk " | cut -f4 -d" ")
        menu+=("$disk" "$size")
    done >/dev/null
    
CHOICE=$(dialog --clear \
		--no-cancel \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${menu[@]}" \
                2>&1 >/dev/tty)

		clear
		#echo "seçilen disk $CHOICE "
		return $CHOICE
}

###### select install_type ######
select_install_type(){
HEIGHT=15
WIDTH=50
CHOICE_HEIGHT=4
BACKTITLE="Backtitle here"
TITLE="Choose Install Type"
MENU="Choose one of the following Install type:"

type=(1 "Diskin Tamamını Kullan"
      2 "Diski Elle Yapılandır")
    
CHOICE=$(dialog --clear \
		--no-cancel \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${type[@]}" \
                2>&1 >/dev/tty)
                #echo "seçilen kurulum tercihi $CHOICE "
                
                return $CHOICE
}

select_filesystem(){
HEIGHT=15
WIDTH=50
CHOICE_HEIGHT=4
BACKTITLE="Backtitle here"
TITLE="Choose filesystem"
MENU="Choose one of the following filesystem:"

filesystem=(1 "ext4"
      2 "ntfs"
      3 "fat32")
    
CHOICE=$(dialog --clear \
		--no-cancel \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${filesystem[@]}" \
                2>&1 >/dev/tty)
                #echo "seçilen dosya sistemi $CHOICE "
                
                return $CHOICE
}
###### part format yes/no  ######
select_format(){
HEIGHT=15
WIDTH=50
CHOICE_HEIGHT=4
BACKTITLE="Backtitle here"
TITLE="Choose format"
MENU="Choose one of the following format partition:"

    
dialog --stdout --title "What to do?" \
  --backtitle "format yes/no" \
  --yesno "Are you sure format the partition " 7 60
dialog_status=$?
#dialog_status==0 Yes

if [ "$dialog_status" -eq 0 ]; then
  # The previous dialog was answered Yes
 echo "yes"
else
  # The previous dialog was answered No or interrupted with <C-c>
  echo "no"
 fi 
                #return $CHOICE
}

###### grub instal  yes/no  ######
select_grub_install(){
HEIGHT=15
WIDTH=50
CHOICE_HEIGHT=4
BACKTITLE="Backtitle here"
TITLE="Choose grub install"
MENU="Choose one of the following grub install:"

dialog --stdout --title "What to do?" \
  --backtitle "grub yes/no" \
  --yesno "Do you want to install boot loader GRUB" 7 60
dialog_status=$?
#dialog_status==0 Yes

if [ "$dialog_status" -eq 0 ]; then
  # The previous dialog was answered Yes
 echo "yes"
else
  # The previous dialog was answered No or interrupted with <C-c>
  echo "no"
 fi 
                #return $CHOICE
}
###### rsync progressbar ######
copy(){
    rsync --no-i-r -a --info=progress2 "$1" "$2" | tr '\r' '\n' | tr -s " " | while read line ; do
        echo "$line" | cut -f2 -d" " | sed "s/%//g"
    done | dialog --gauge "$TITLE" 0 0
}

#işlemler
#select_install_type
#result_install_type=$?
#echo "seçilen kurulum tercihi $result_install_type "

#select_disk1
#result_disk=$?
#echo "seçilen disk $result_disk "

#select_filesystem
#result_filesystem=$?
#echo "seçilen disk $result_filesystem "

#select_format
#result_filesystem=$?
#echo "seçilen format $result_format "

#select_grub_install
#result_grub=$?
#echo "seçilen grub $result_grub "


#select_partition
#result_partition=$?
#echo "seçilen disk $result_partition "

